import { Animate } from 'jupiterui-components';
import { useTheme } from 'next-themes';
import { useEffect } from 'react';
import Image from 'next/image';

const Home = (props) => {
	const { setTheme } = useTheme();
	const hours = new Date().getHours();

	useEffect(() => {
		console.log(`Timezone indicates your 24-hour time to be: ${hours}`);

		if (hours > 6 && hours < 20) {
			setTheme('light');
		} else {
			setTheme('dark');
		}
	}, [props, hours]);

	const projects = [
		{
			title: 'JupiterUI',
			description:
				"A modern, open-source UI kit that's flexible and extendable.",
			to: 'https://codeberg.org/JupiterUI/JupiterUI',
			a: 'justify-e align-e'
		},
		{
			title: 'FSUtil',
			description:
				"File System Utility (FSUtil), is a useful file system module for no-brainer file and directory operations.",
			to: 'https://codeberg.org/FiveAtoms/FSUtil',
			a: 'justify-e align-s'
		},
		{
			title: 'Hydrogen',
			description:
				"A mini file that helps improve DOM operation experience with better methods and better requests.",
			to: 'https://codeberg.org/FiveAtoms/Hydrogen',
			a: 'justify-s align-e'
		},
		{
			title: 'SimpleQueue',
			description:
				"A dead-simple Queue data type.",
			to: 'https://codeberg.org/FiveAtoms/SimpleQueue',
			a: 'justify-s align-s'
		},
	];

	return (
		<section className='section mnh-screen flex-c ui-1 dark-bg-gray-100'>
			<div className='container flex-c'>
				<div className='w-100p flex-c'>
					<div className='mw-40r flex-c text-c'>
						<Animate
							animate={'scale-in-up'}
							className='delay-200 duration-800'
						>
							<div className='flex flex-row align-c'>
								<Image
									src={'/media/infinium-green.svg'}
									width={'40'}
									height={'40'}
								/>
								<span className='text-upper fs-xl ls-3xl ml-1r'>
									Infinium{' '}
									<span className='text-accent'>Earth</span>
								</span>
							</div>
						</Animate>

						{/* <Animate
							animate={'scale-in-up'}
							className='delay-400 duration-800'
						>
							<h1 className='fs-8xl fw-600 tablet-fs-7xl landscape-fs-6xl'>
								Software,{' '}
								<span className='text-accent'>elevated.</span>
							</h1>
						</Animate> */}

						<Animate
							animate={'scale-in-up'}
							className='delay-400 duration-800 mt-2r'
						>
							<p className="text-dynamic-08 fs-3xl fw-400 lh-1-7">Currently, we are only accepting new clients and opportunities through known referrals.</p>
						</Animate>
					</div>
				</div>
				{/* <div className='grid-block align-c'>
					<div className='mw-50r p-2r'>
						<Animate
							animate={'scale-in-up'}
							className='delay-1000 duration-800'
						>
							<h3>Open source projects</h3>
						</Animate>
						<div className='w-100p grid grid-2 tablet-grid-2 landscape-grid-1'>
							{projects.map((e, i) => (
								<div className={`grid-block ${e.a}`}>
									<Animate
										animate={'scale-in-up'}
										className={`duration-800 delay-${
											1200 + i * 200
										}`}
									>
										<Card
											className='dark-bg-gray-90 clickable'
											title={e.title}
											content={<p>{e.description}</p>}
											onClick={() => window.open(e.to, '_blank', 'noreferrer=yes')}
										/>
									</Animate>
								</div>
							))}
						</div>
					</div>
				</div> */}
			</div>
		</section>
	);
};

export default Home;
