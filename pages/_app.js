import { ThemeProvider } from 'next-themes';

import '../styles/jupiterui.css';
import '../styles/jupiterui-overrides.css';

const App = ({ Component, pageProps }) => {
	return (
		<ThemeProvider>
			<Component {...pageProps} />
		</ThemeProvider>
	);
};

export default App;
